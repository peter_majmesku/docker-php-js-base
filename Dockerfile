FROM ubuntu:17.10
MAINTAINER Peter Majmesku

RUN apt-get update
RUN apt-get install -y wget bzip2 curl sqlite3 screen
RUN (cd /tmp && wget -qO- https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 | tar jx && chmod +x /tmp/phantomjs-2.1.1-linux-x86_64/bin/phantomjs)
RUN apt-get install -y unzip git curl php php-cli php-gd php-curl php7.1-sqlite
RUN apt-get install -y php-dom php-json php-common php-mbstring
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer global require drush/drush:^9
RUN curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
RUN apt-get update
RUN apt-get install -y nodejs npm